#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Servidor."""

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import sys
import os
import socketserver


class XMLHandler(ContentHandler):
    """Clase para manejar xml."""

    def __init__(self):
        """Constructor. Inicializamos las variables."""
        self.lista_etiquetas = []
        self.dic = {'account': ['username', 'passwd'],
                    'uaserver': ['ip', 'puerto'],
                    'rtpaudio': ['puerto'],
                    'regproxy': ['ip', 'puerto'],
                    'log': ['path'],
                    'audio': ['path']}

    def startElement(self, name, attrs):
        """Almacenar la informacion."""
        if name in self.dic:
            dicc = {}
            for item in self.dic[name]:
                dicc[item] = attrs.get(item, "")
            diccname = {name: dicc}
            self.lista_etiquetas.append(diccname)

    def get_tags(self):
        """Método que devuelve las etiquetas."""
        return self.lista_etiquetas


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    dicc = {}

    def handle(self):
        """handle."""
        bad_req = b"SIP/2.0 400 Bad Request\r\n\r\n"
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            MENSAJE = line.decode('utf-8')
            self.puerto_r = ' '
            if MENSAJE != " ":
                print("El cliente nos manda " + line.decode('utf-8'))
                recibido = line.decode('utf-8')
                PETICION = MENSAJE.split(' ')

                allow = ['INVITE', 'ACK', 'BYE', '']

                if PETICION[0] == 'INVITE':
                    mensaje = "SIP/2.0 100 Trying\r\n\r\n"
                    mensaje += "SIP/2.0 180 Ringing\r\n\r\n"
                    mensaje += "SIP/2.0 200 OK "
                    self.dicc['puerto'] = PETICION[7]

                    # SDP
                    account_user = PETICION[3].split('=')[2]
                    mensaje += "SIP/2.0\r\nContent-Type:"
                    mensaje += " application/sdp\r\n\r\n"
                    mensaje += "v=0\r\n" + "o=" + account_user + " "
                    mensaje += PETICION[4] + " IN IP4\r\n"
                    mensaje += "s=misesion\r\n" + "t=0\r\nm=audio "
                    mensaje += rtp_port + " RTP\r\n"
                    self.wfile.write(bytes(mensaje, 'utf-8'))

                if PETICION[0] == 'ACK':
                    print('EL PUERTO ES' + self.dicc['puerto'])
                    ejecutar = "mp32rtp -i " + proxy_ip + " -p "
                    ejecutar += self.dicc['puerto'] + " < "
                    ejecutar += audio_path
                    print("se va a ejecutar el programa")
                    os.system(ejecutar)
                    # Mensaje en el log
                elif PETICION[0] == 'BYE':
                        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                        # Mensaje en el log

                elif PETICION[0] not in allow:
                        print(PETICION)
                        self.wfile.write(b"SIP/2.0 405 Method Not Allowed")
                elif PETICION[0] == '':
                    pass

            if not line:
                break


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    if len(sys.argv) != 2:
        sys.exit('Usage: python3 server.py config')
    else:
        file = sys.argv[1]
        if os.path.exists(file):
            print("Listening...")
        else:
            sys.exit('Usage: python3 server.py config')

    parser = make_parser()
    cHandler = XMLHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(file))
    lista = cHandler.get_tags()

    server_ip = lista[1]['uaserver']['ip']
    server_port = lista[1]['uaserver']['puerto']
    rtp_port = lista[2]['rtpaudio']['puerto']
    proxy_ip = lista[3]['regproxy']['ip']
    audio_path = lista[5]['audio']['path']
    try:
        serv = socketserver.UDPServer((server_ip,
                                       int(server_port)), EchoHandler)
        print("Lanzando servidor UDP de eco...")
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
