#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Proxy."""

import sys
import os
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import socketserver
import time
import json
import socket
import random
import hashlib


class XMLHandler(ContentHandler):
    """Manejo del xml."""

    def __init__(self):
        """Init."""
        self.lista_etiquetas = []
        self.dic = {'server': ['name', 'ip', 'puerto'],
                    'database': ['path', 'passwdpath'],
                    'log': ['path']}

    def startElement(self, name, attrs):
        """xml a diccionario."""
        if name in self.dic:
            dicc = {}
            for item in self.dic[name]:
                dicc[item] = attrs.get(item, "")
            diccname = {name: dicc}
            self.lista_etiquetas.append(diccname)

    def get_tags(self):
        """Devuelve las etiquetas."""
        return self.lista_etiquetas


class ProxyHandle(socketserver.DatagramRequestHandler):
    """Funciones del proxy."""

    sip_address = {}
    dicc_nonce = {}

    def expired_registered(self):
        """method of the server class."""
        lista = []
        try:
            with open(database_path, 'r') as file:
                self.sip_address = json.load(file)
                if self.sip_address:
                    for element in self.sip_address:
                        data = self.sip_address[element]
                        print(element)
                        login_time = data['hora']

                        tiempo_st = time.strptime(login_time,
                                                  '%Y-%m-%d %H:%M:%S')

                        start = time.mktime(tiempo_st)
                        expire = data['expira']
                        total = start + float(expire)

                        actual_time = time.strftime('%Y-%m-%d %H:%M:%S',
                                                    time.localtime())
                        actual_time_st = time.strptime(actual_time,
                                                       '%Y-%m-%d %H:%M:%S')
                        actual_time_seg = time.mktime(actual_time_st)

                        if actual_time_seg > total:
                            lista.append(element)

                    self.eliminado = 0
                    for usuario in lista:
                        del self.sip_address[usuario]
                        self.eliminado = 1
                else:
                    pass

        except FileNotFoundError:
            pass

    def register2json(self):
        """Pasa los datos al archivo."""
        name = database_path
        # Añado el tiempo
        with open(name, 'w') as file:
            json.dump(self.sip_address, file, indent=4)

    def check_user(self, user):
        """Comprueba el usuario."""
        if user not in self.sip_address:
            self.found = 0
        else:
            self.found = 1
        return self.found

    def check_pass(self, aRegistrar, client_pass):
        """Comprueba la contraseña."""
        fichero = open(database_pass, 'r')
        lineas = fichero.readlines()
        self.registered = 0

        for linea in lineas:
            slices = linea.split(' ')
            usuario = slices[1]
            password = slices[3].split('\n')[0]

            new_nonce = self.dicc_nonce[aRegistrar]
            m = hashlib.md5()
            m.update(bytes(password + str(new_nonce), 'utf-8'))
            encripted = m.hexdigest()

            if usuario == aRegistrar:
                if encripted == client_pass:
                    self.registered = 1
                else:
                    self.registered = 0

        fichero.close()
        return self.registered

    def write_log(self, depuracion, ip, port, line):
        """Escribe en el log."""
        with open(log_path, 'a') as fich:
            hora = time.strftime('%Y%m%d%H%M%S', time.localtime())
            puerto = str(port)
            if depuracion == 'Starting...' or depuracion == 'Finishing.':
                write = str(hora) + ' ' + depuracion + '\r\n'
            elif line == '':
                write = ''
            else:
                if line != '\r\n\r\n' or line != '' or line != ' ':
                    jump = line.replace('\r\n', ' ')
                    write = str(hora) + ' ' + depuracion + ' ' + ip
                    write += ':' + str(puerto) + ': ' + str(jump) + '\r\n'

            fich.write(write)
            fich.close()

    def send_client(self, line):
        """Envia al cliente."""
        self.wfile.write(bytes(line, 'utf-8'))
        print('Le envio al cliente: \r\n' + line)
        depuracion = 'sent to'
        ProxyHandle.write_log(self, depuracion, self.user_ip,
                              self.client_port, line)

    def send_server(self, ip, port, line):
        """Envia al servidor."""
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((ip, port))

                    to_server = line
                    print(to_server)
                    my_socket.send(bytes(to_server, 'utf-8'))

                    depuracion = 'sent to'
                    ProxyHandle.write_log(self, depuracion,
                                          ip, port, to_server)

                    # La respuesta del servidor
                    data = my_socket.recv(1024)
                    print('Recibo del server: \r\n' + data.decode('utf-8'))
                    server_to_client = data.decode('utf-8')
                    ProxyHandle.send_client(self, server_to_client)

                    depuracion = 'Received from'
                    ProxyHandle.write_log(self, depuracion, ip,
                                          port, server_to_client)
        except ConnectionRefusedError:
            depuracion = 'Error: No server listening at '
            ProxyHandle.write_log(self, depuracion, self.user_ip,
                                  self.client_port, '.')

            self.wfile.write(bytes(depuracion, 'utf-8'))
            depuracion = 'sent to'
            ProxyHandle.write_log(self, depuracion, self.user_ip,
                                  self.client_port, depuracion)

    def handle(self):
        """handle."""
        bad_req = b"SIP/2.0 400 Bad Request\r\n\r\n"
        not_found = b"SIP/2.0 404 User Not Found\r\n\r\n"
        send_200 = b"SIP/2.0 200 OK\r\n\r\n"
        gone = b"SIP/2.0 410 Gone\r\n\r\n"
        sip_data = {'ip': ' ', 'hora': ' ', 'puerto': ' ', 'expira': ' '}

        ProxyHandle.expired_registered(self)

        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            MENSAJE = line.decode('utf-8')

            PETICION = MENSAJE.split(' ')
            allow = ['INVITE', 'ACK', 'BYE', 'REGISTER']

            self.user_ip = self.client_address[0]
            self.client_port = self.client_address[1]

            depuracion = 'Received from'
            ProxyHandle.write_log(self, depuracion, self.user_ip,
                                  self.client_port, MENSAJE)

            if MENSAJE != "":
                print("El cliente nos manda " + line.decode('utf-8'))
                recibido = line.decode('utf-8')

                if PETICION[0] == 'REGISTER':
                    # Compruebo si tiene la cabecera de autorizacion
                    prueba = recibido.split('\r\n')
                    n = len(prueba)

                    if n <= 4:
                        direccion = PETICION[1].split(':')
                        user_addres = direccion[1]

                        nonce = random.randint(0, 100000000)
                        self.dicc_nonce[user_addres] = nonce

                        send_401 = "SIP/2.0 401 Unauthorized\r\n"
                        send_401 += 'WWW-Authenticate: Digest '
                        send_401 += 'nonce="' + str(nonce) + '"\r\n\r\n'
                        self.wfile.write(bytes(send_401, 'utf-8'))
                    else:
                        # Compruebo si el usuario está en passwords
                        client_encripted = PETICION[5].split('"')[1]

                        direccion = PETICION[1].split(':')
                        user_addres = direccion[1]
                        user_port = direccion[2]
                        opcion = PETICION[2].split('\r\n')[1]
                        ProxyHandle.check_pass(self, user_addres,
                                               client_encripted)

                        if self.registered == 1:
                            if opcion == 'Expires:':

                                value = PETICION[3].split('\r\n')
                                try:
                                    tiempo = int(value[0])
                                except ValueError:
                                    self.wfile.write(bad_req)
                                    break

                                if tiempo == 0:
                                    if user_addres not in self.sip_address:
                                        self.wfile.write(not_found)
                                    else:
                                        del self.sip_address[user_addres]
                                        self.wfile.write(send_200)
                                        ProxyHandle.register2json(self)

                                elif tiempo < 0:
                                    self.wfile.write(bad_req)
                                else:
                                    final = time.strftime('%Y-%m-%d %H:%M:%S',
                                                          time.localtime())
                                    sip_data['expira'] = str(tiempo)
                                    sip_data['hora'] = final
                                    sip_data['ip'] = self.client_address[0]
                                    sip_data['puerto'] = user_port
                                    self.sip_address[user_addres] = sip_data
                                    self.wfile.write(send_200)
                                    ProxyHandle.register2json(self)
                        else:
                            self.wfile.write(b'Acceso denegado')

                if PETICION[0] in allow and not PETICION[0] == 'REGISTER':
                    usuario = PETICION[1].split(':')[1]
                    ProxyHandle.check_user(self, usuario)
                    if self.found == 0:
                        if self.eliminado == 1:
                            answer = self.wfile.write(gone)
                        else:
                            answer = self.wfile.write(not_found)

                    elif self.found == 1:
                        ip = self.sip_address[usuario]['ip']
                        port = self.sip_address[usuario]['puerto']
                        ProxyHandle.send_server(self, ip, int(port), recibido)
                elif PETICION[0] not in allow:
                        self.wfile.write(b"SIP/2.0 405 Method Not Allowed")

            if not line:
                break


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    if len(sys.argv) != 2:
        sys.exit('Usage: python3 proxy_registrar.py config')
    else:
        file = sys.argv[1]
        if not os.path.exists(file):
            sys.exit('Usage: python3 proxy_registrar.py config')
        else:
            parser = make_parser()
            cHandler = XMLHandler()
            parser.setContentHandler(cHandler)
            parser.parse(open(file))
            lista = cHandler.get_tags()
            # print(lista)

            server_port = lista[0]['server']['puerto']
            database_path = lista[1]['database']['path']
            server_name = lista[0]['server']['name']
            server_ip = lista[0]['server']['ip']
            database_pass = lista[1]['database']['passwdpath']
            log_path = lista[2]['log']['path']

            init = "Server " + server_name
            init += " listening at port " + server_port
            print(init)

        if server_ip == '':
            ip = '127.0.0.1'
        else:
            ip = server_ip

        port = int(server_port)
        try:
            serv = socketserver.UDPServer((ip, int(server_port)), ProxyHandle)
            serv.serve_forever()
        except KeyboardInterrupt:
            print("Finalizado servidor")
