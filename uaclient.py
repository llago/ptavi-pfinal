#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys
import os
import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import hashlib


class Ua1(ContentHandler):
    """Client handler."""

    def __init__(self):
        """Init."""
        self.lista_etiquetas = []
        self.dic = {'account': ['username', 'passwd'],
                    'uaserver': ['ip', 'puerto'],
                    'rtpaudio': ['puerto'],
                    'regproxy': ['ip', 'puerto'],
                    'log': ['path'],
                    'audio': ['path']}

    def startElement(self, name, attrs):
        """Xml a diccionario."""
        if name in self.dic:
            dicc = {}
            for item in self.dic[name]:
                dicc[item] = attrs.get(item, "")
            diccname = {name: dicc}
            self.lista_etiquetas.append(diccname)

    def get_tags(self):
        """tags."""
        return self.lista_etiquetas


class data_log():
    """fichero log."""

    def write_log(self, log, depuracion, ip, port, line):
        """Escribe el fichero."""
        file = config.split('.')[0]
        with open(file + '_log.txt', 'a') as fich:
            hora = time.strftime('%Y%m%d%H%M%S', time.localtime())
            puerto = str(port)
            if depuracion == 'Starting...' or depuracion == 'Finishing.':
                write = str(hora) + ' ' + depuracion + '\r\n'
            else:
                jump = line.replace('\r\n', ' ')
                write = str(hora) + ' ' + depuracion + ' '
                write += ip + ':' + str(puerto) + ': ' + str(jump) + '\r\n'

            fich.write(write)
            fich.close()


if __name__ == "__main__":

    exit = 'Usage: python3 uaclient.py config method opcion'

    if len(sys.argv) != 4:
        sys.exit(exit)
    else:

        config = sys.argv[1]
        method = sys.argv[2].upper()
        option = sys.argv[3]
        metodos = ['REGISTER', 'INVITE', 'BYE']

        if method not in metodos:
            sys.exit(exit)

        if os.path.exists(config) is False:
            sys.exit("This name of file doesn´t exist")

        else:
            parser = make_parser()
            cHandler = Ua1()
            parser.setContentHandler(cHandler)
            parser.parse(open(config))
            lista = cHandler.get_tags()
            log = data_log()

            account_user = lista[0]['account']['username']
            account_pass = lista[0]['account']['passwd']

            server_ip = lista[1]['uaserver']['ip']
            server_port = lista[1]['uaserver']['puerto']

            rtp_port = lista[2]['rtpaudio']['puerto']

            proxy_ip = lista[3]['regproxy']['ip']
            proxy_port = lista[3]['regproxy']['puerto']

            log_path = lista[4]['log']['path']
            audio_path = lista[5]['audio']['path']

        sip_line = method + " sip:"
        if method == 'REGISTER':
            sip_line += account_user + ":" + server_port
            sip_line += " SIP/2.0\r\nExpires: " + option + '\r\n'
            depuracion = 'Starting...'
            log.write_log(log_path, depuracion, '', '', '')
        elif method == 'INVITE':
            sip_line += option + " SIP/2.0\r\nContent-Type:"
            sip_line += "application/sdp\r\n\r\n"
            sip_line += "v=0\r\n" + "o=" + account_user + " "
            sip_line += server_ip + " IN IP4\r\n"
            sip_line += "s=misesion\r\n" + "t=0\r\nm=audio "
            sip_line += rtp_port + " RTP"
            print(sip_line)
        elif method == 'BYE':
            sip_line += option + " SIP/2.0\r\n"
            print(sip_line)

            depuracion = 'Finishing.'
            log.write_log(log_path, depuracion, '', '', '')

        SERVER = proxy_ip
        PORT = int(proxy_port)

        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                my_socket.connect((SERVER, PORT))

                my_socket.send(bytes(sip_line, 'utf-8') + b'\r\n')
                print(sip_line)
                depuracion = 'sent to'
                log.write_log(log_path, depuracion, SERVER, PORT, sip_line)
                data = my_socket.recv(1024)
                print('He recibido: \r\n' + data.decode('utf-8'))
                received = data.decode('utf-8')

                depuracion = 'Received from'
                log.write_log(log_path, depuracion, SERVER,
                              PORT, data.decode('utf-8'))

                status = received.split('\r\n')
                to_ack = ["SIP/2.0 100 Trying", "SIP/2.0 180 Ringing",
                          "SIP/2.0 200 OK"]

                if status[0] == "SIP/2.0 401 Unauthorized":
                    nonce = received.split('"')[1]
                    print(nonce)

                    m = hashlib.md5()
                    m.update(bytes(account_pass + nonce, 'utf-8'))
                    encripted = m.hexdigest()

                    autorizacion = 'Authorization: Digest response="'
                    autorizacion += encripted + '"\r\n'
                    sip_line += autorizacion

                    my_socket.send(bytes(sip_line, 'utf-8') + b'\r\n')
                    print(sip_line)
                    depuracion = 'sent to'
                    log.write_log(log_path, depuracion, SERVER, PORT, sip_line)
                    data = my_socket.recv(1024)
                    print(data.decode('utf-8'))
                    received = data.decode('utf-8')

                    depuracion = 'Received from'
                    log.write_log(log_path, depuracion, SERVER,
                                  PORT, data.decode('utf-8'))

                elif status[0] == to_ack[0]:
                    ua2_rtp = received.split(' ')[12]
                    # Envio ACK
                    sip_line = "ACK" + " sip:" + option + " SIP/2.0\r\n"
                    my_socket.send(bytes(sip_line, 'utf-8') + b'\r\n')
                    # Añado al log lo que envío
                    depuracion = 'sent to'
                    log.write_log(log_path, depuracion, SERVER, PORT, sip_line)
                    # Envio rtp
                    ejecutar = "mp32rtp -i " + proxy_ip +
                    ejecutar += " -p " + ua2_rtp + " < "
                    ejecutar += audio_path
                    print("se va a ejecutar el programa")
                    os.system(ejecutar)

        except ConnectionRefusedError:
            depuracion = 'Error: No server listening at '
            log.write_log(log_path, depuracion, SERVER, PORT, ' ')
            sys.exit('Error: No server listening')
